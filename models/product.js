var restful  = require("node-restful");
var mongoose = restful.mongoose

var productSchema = new mongoose.Schema({
	name:String,
	age:Number,
	dept:String
});

module.exports = restful.model("Products",productSchema);